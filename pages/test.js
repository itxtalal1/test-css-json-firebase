import React from "react";
import { doc, getDoc } from "firebase/firestore";
import { db } from "../firebase";
import { toCSS, toJSON } from "css-convert-json";

const Test = () => {
  const [css, setCss] = React.useState("");

  const getData = async () => {
    const ref = doc(db, "cssTest", "1");
    const docSnap = await getDoc(ref);
    if (docSnap.exists()) {
      // Convert to City object
      const json = docSnap.data();
      // Use a City instance method
      console.log(json);
      const css = toCSS(json);
      setCss(css);
    } else {
      console.log("No such document!");
    }
  };

  getData();

  return (
    <div>
      <h1 className="hi">Test</h1>

      <style jsx global>
        {`
          ${css}
        `}
      </style>
    </div>
  );
};

export default Test;
