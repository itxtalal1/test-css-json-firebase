import { useEffect, useState } from "react";
import { toCSS, toJSON } from "css-convert-json";
import { doc, setDoc } from "firebase/firestore";
import { db } from "../firebase";
import Link from "next/link";

export default function Home() {
  const [css, setCss] = useState("");

  const addData = async (data) => {
    await setDoc(doc(db, "cssTest", "1"), data);
  };

  const submitHandler = async (e) => {
    e.preventDefault();
    const val = document.getElementById("css").value;

    const json = toJSON(val);
    console.log("json", json);

    addData(json);

    document.getElementById("css").value = "";

    const cnss = toCSS(json);
    console.log("css", cnss);
    // setCss(cnss);
  };

  return (
    <div>
      <h1>Index Page</h1>
      <form onSubmit={submitHandler}>
        <label htmlFor="css">CSS</label>
        <textarea
          name="css"
          id="css"
          cols="30"
          rows="10"
          placeholder="Enter your CSS here"
        ></textarea>
        <button type="submit">Submit</button>
      </form>

      <Link href="/test">Click me to check the CSS after submitting !!</Link>

      <style jsx global>
        {`
          ${css}
        `}
      </style>
    </div>
  );
}
