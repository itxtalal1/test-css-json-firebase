// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDCoIgNv_9pQzVlslFZD0QBLhvN6XmLTwQ",
  authDomain: "williams-app-1c72e.firebaseapp.com",
  projectId: "williams-app-1c72e",
  storageBucket: "williams-app-1c72e.appspot.com",
  messagingSenderId: "102834624912",
  appId: "1:102834624912:web:048faf7e0a3d032cb03b15",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export { db };
